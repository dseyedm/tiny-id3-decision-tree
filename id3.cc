//
// implementation of the ID3 algorithm.
// discrete data only.
// sorajsm
//

#include <bits/stdc++.h>
#include <stdint.h>
#include <assert.h>
#include <math.h>

using namespace std;

//
// |----------header-----------|
// xes,    as,    0s,  classes
// |------data------||-classes-|
// XXXX, AAAA, 00000,  CLASS0
// YYYY, BBBB, 11111,  CLASS1
// ....
//
#define DATA_CHECKBOUNDS 0
struct data_t {
	vector<string> m_header;

	//
	// stores only unique strings found in the data portion. (likewise for the classes portion)
	// this is to speed up comparisons -- better to do fast O(1) comparisons instead of O(n) all of the time.
	//
	vector<string> m_unique_datums;
	vector<string> m_unique_classes;

	vector<vector<int> > m_data;
	vector<int> m_classes;

	inline const string& header(int i) const {
#if DATA_CHECKBOUNDS
		assert(0 <= i && i < (int)m_header.size());
#endif
		return m_header[i];
	}

	inline int data_i(int r, int c) const {
#if DATA_CHECKBOUNDS
		assert(0 <= r && r < (int)m_data.size());
		assert(0 <= c && c < (int)m_data[r].size());
#endif
		return m_data[r][c];
	}

	inline const string& data_s(int r, int c) const {
#if DATA_CHECKBOUNDS
		assert(0 <= r && r < (int)m_data.size());
		assert(0 <= c && c < (int)m_data[r].size());
		int tmp_data_i = m_data[r][c];
		assert(0 <= tmp_data_i && tmp_data_i < (int)m_unique_datums.size());
		return m_unique_datums[tmp_data_i];
#else
		return m_unique_datums[m_data[r][c]];
#endif
	}

	inline int class_i(int r) const {
#if DATA_CHECKBOUNDS
		assert(0 <= r && r < (int)m_classes.size());
#endif
		return m_classes[r];
	}

	inline const string& class_s(int r) const {
#if DATA_CHECKBOUNDS
		assert(0 <= r && r < (int)m_classes.size());
		int tmp_class_i = m_classes[r];
		assert(0 <= tmp_class_i && tmp_class_i < (int)m_unique_classes.size());
		return m_unique_classes[tmp_class_i];
#else
		return m_unique_classes[m_classes[r]];
#endif
	}

	void print() const {
		for(int i = 0; i < (int)m_header.size(); ++i) {
			printf("%s\t", header(i).c_str());
		}
		printf("\n");
		for(int i = 0; i < (int)m_data.size(); ++i) {
			for(int j = 0; j < (int)m_data[i].size(); ++j) {
				printf("%s\t", data_s(i, j).c_str());
			}
			printf("%s\n", class_s(i).c_str());
		}
	}
};
#undef DATA_CHECKBOUNDS

//
// load_data assumes that out_d has not been loaded before.
//
bool load_data(data_t& out_d, stringstream& ss, const char delim = ',') {
	//
	// consistent_column_count is to ensure the same # of columns across rows.
	//
	int consistent_column_count = -1;

	//
	// read the header.
	//
	string line;
	if(getline(ss, line)) {
		stringstream line_ss; line_ss << line;
		string datum;
		int column_count = 0;
		while(getline(line_ss, datum, delim)) {
			++column_count;
			out_d.m_header.push_back(datum);
		}
		consistent_column_count = column_count;
	}

	//
	// read [data0...dataN][class]
	//
	map<string, int> unique_datums;
	map<string, int> unique_classes;
	while(getline(ss, line)) {
		out_d.m_data.push_back(vector<int>());
		out_d.m_data.back().reserve(consistent_column_count/* - 1*/);

		stringstream line_ss; line_ss << line;
		string datum;
		int column_count = 0;
		while(getline(line_ss, datum, delim)) {
			//
			// check if this datum is not a class.
			//
			if(++column_count < consistent_column_count) {
				vector<int>& v = out_d.m_data.back();
				const map<string, int>::iterator it = unique_datums.find(datum);
				if(it != unique_datums.end()) {
					v.push_back(it->second);
				} else {
					unique_datums.insert(make_pair<string, int>(
						datum,
						out_d.m_unique_datums.size()
					));
					v.push_back(out_d.m_unique_datums.size());
					out_d.m_unique_datums.push_back(datum);
				}
			} else {
				vector<int>& v = out_d.m_classes;
				const map<string, int>::iterator it = unique_classes.find(datum);
				if(it != unique_classes.end()) {
					v.push_back(it->second);
				} else {
					unique_classes.insert(make_pair<string, int>(
						datum,
						out_d.m_unique_classes.size()
					));
					v.push_back(out_d.m_unique_classes.size());
					out_d.m_unique_classes.push_back(datum);
				}
			}
		}
		//
		// return false if the column counts are not consistent.
		//
		if(column_count != consistent_column_count) return false;
	}

	//
	// must have at least two columns and at least one row.
	//
	return consistent_column_count > 1 && out_d.m_data.size();
}

struct id3_node_t {
	struct id3_branch_t {
		string datum;
		id3_node_t* pointer;
	};
	//
	// this a leaf node when children.size() == 0.
	//
	vector<id3_branch_t> children;

	//
	// if this node is a leaf node, label stores the predicted class.
	// if this node is not a leaf node, label stores the attribute to split on.
	//
	string label;
};

//
// to avoid copying the table each recursion, keep track of what
// rows each call is working on and keep the table data constant.
//
#define ID3_ASSERT 0
id3_node_t* create_id3_subtree(
	const data_t& d,
	const vector<int>& rows,
	const vector<int>& columns) // does not include the class column
{
	if(rows.size() <= 0 || columns.size() <= 0) return NULL;
	const int n_rows = (int)rows.size();
	const int n_cols = (int)columns.size();

	//
	// O(n) check for a pure/homogenous table.
	//
	bool pure = true; {
		int consistent_class = d.class_i(rows[0]);
		for(int row_i = 1; row_i < n_rows; ++row_i) {
			int data_row_i = rows[row_i];
			if(consistent_class == d.class_i(data_row_i)) continue;
			pure = false;
			break;
		}
	}
	if(pure) {
		id3_node_t* leaf = new id3_node_t;
		int data_row_i = rows[0];
		leaf->label = d.class_s(data_row_i);
		return leaf;
	}

	//
	// calculate entropy_class.
	//
	float entropy_class = 0.0f; { // = sum(-P(c)log2 P(c))
		vector<int> class_count(d.m_unique_classes.size(), 0);
		for(int row_i = 0; row_i < n_rows; ++row_i) {
			int data_row_i = rows[row_i];
			++class_count[d.class_i(data_row_i)];
		}
		for(int i = 0; i < (int)class_count.size(); ++i) {
			if(!class_count[i]) continue;
			float probability_class = (float)class_count[i]/(float)n_rows;
			entropy_class -= probability_class*log2f(probability_class);
		}
	}

	//
	// choose the attribute which has the greatest information gain.
	// Igain = Eclass - E(P(d)*Esubclass)
	//
	float max_information_gain = -1.0f;
	int max_ig_column_i = -1; // _data_ column
	for(int col_i = 0; col_i < n_cols; ++col_i) {
		int data_column_i = columns[col_i];
		//
		// collect the row indices for each unique datum.
		//
		vector<vector<int> > datum_indices; // _data_ row indices
		map<int, int> datum_to_index;
		for(int row_j = 0; row_j < n_rows; ++row_j) {
			int data_row_j = rows[row_j];

			int datum = d.data_i(data_row_j, data_column_i);
			const map<int, int>::iterator it = datum_to_index.find(datum);
			if(it != datum_to_index.end()) {
				datum_indices[it->second].push_back(data_row_j);
			} else {
				datum_to_index.insert(make_pair<int, int>(datum, (int)datum_indices.size()));
				datum_indices.push_back(vector<int>(1, data_row_j));
			}
		}
		//
		// calculate information gain from splitting on this particular column.
		//
		float information_gain = entropy_class;
		for(int datum_i = 0; datum_i < (int)datum_indices.size(); ++datum_i) {
#if ID3_ASSERT
			assert(datum_indices[datum_i].size());
#endif
			float entropy_sub_class = 0.0f; { // = sum(-P(c)log2 P(c))
				vector<int> class_count(d.m_unique_classes.size(), 0);
				for(int datum_j = 0; datum_j < (int)datum_indices[datum_i].size(); ++datum_j) {
					++class_count[d.class_i(datum_indices[datum_i][datum_j]/* _data_ row index */)];
				}
				for(int cc_j = 0; cc_j < (int)class_count.size(); ++cc_j) {
					if(!class_count[cc_j]) continue;
					float probability_sub_class = (float)class_count[cc_j]/(float)datum_indices[datum_i].size();
					entropy_sub_class -= probability_sub_class*log2f(probability_sub_class);
				}
			}
#if ID3_ASSERT
			assert(n_rows);
#endif
			float probability_datum = (float)datum_indices[datum_i].size()/(float)n_rows;
			information_gain -= probability_datum*entropy_sub_class;
		}
		if(information_gain > max_information_gain) {
			max_information_gain = information_gain;
			max_ig_column_i = data_column_i;
		}
	}
#if ID3_ASSERT
	assert(max_ig_column_i >= 0);
#endif

	//
	// recover the datum row indices for the best splitting column.
	// this is likely faster than copying it each time we find a more
	// suitable splitting candidate in the loop above.
	//
	vector<vector<int> > datum_indices; // _data_ row indices
	map<int, int> datum_to_index;
	for(int row_j = 0; row_j < n_rows; ++row_j) {
		int data_row_j = rows[row_j];

		int datum = d.data_i(data_row_j, max_ig_column_i);
		const map<int, int>::iterator it = datum_to_index.find(datum);
		if(it != datum_to_index.end()) {
			datum_indices[it->second].push_back(data_row_j);
		} else {
			datum_to_index.insert(make_pair<int, int>(datum, (int)datum_indices.size()));
			datum_indices.push_back(vector<int>(1, data_row_j));
		}
	}

	//
	// do not include the best splitting column.
	//
	vector<int> columns_sub; columns_sub.reserve(columns.size()/* - 1 */);
	for(int col_i = 0; col_i < (int)columns.size(); ++col_i) {
		if(columns[col_i] == max_ig_column_i) continue;
		columns_sub.push_back(columns[col_i]);
	}

	//
	// recurse on each unique datum and its subsection of the data table.
	//
	id3_node_t* branch = new id3_node_t;
	branch->label = d.header(max_ig_column_i/* _data_ column */);
	branch->children.resize(datum_indices.size());
	for(int datum_i = 0; datum_i < (int)datum_indices.size(); ++datum_i) {
		const vector<int>& rows_sub = datum_indices[datum_i];
		branch->children[datum_i].datum = d.data_s(
			datum_indices[datum_i][0], // _data_ index
			max_ig_column_i // _data_ index
		);
		branch->children[datum_i].pointer = create_id3_subtree(
			d,
			rows_sub,
			columns_sub
		);
	}
	return branch;
}
#undef ID3_ASSERT

id3_node_t* create_id3_tree(const data_t& d) {
	if(!d.m_data.size() ||
	   !d.m_data[0].size() ||
	   !d.m_header.size() ||
	   !d.m_classes.size()) return NULL;

	vector<int> columns(d.m_data[0].size());
	vector<int> rows(d.m_data.size());

	for(int col_i = 0; col_i < (int)columns.size(); ++col_i) columns[col_i] = col_i;
	for(int row_i = 0; row_i < (int)rows.size(); ++row_i) rows[row_i] = row_i;

	return create_id3_subtree(d, rows, columns);
}

#define PAD for(int d = 0; d < depth; ++d) printf("|  ")
void print_id3_tree(const id3_node_t* node, int depth = 0) {
	if(!node) return;
	if(node->children.size()) { // branch
		PAD; printf("split: %s\n", node->label.c_str());
		for(int child_i = 0; child_i < (int)node->children.size(); ++child_i) {
			PAD; printf("branch: %s\n", node->children[child_i].datum.c_str());
			print_id3_tree(node->children[child_i].pointer, depth + 1);
		}
	} else { // leaf
		PAD; printf("prediction: %s\n", node->label.c_str());
	}
}
#undef PAD

void destroy_id3_tree(const id3_node_t* node) {
	if(!node) return;
	for(int child_i = 0; child_i < (int)node->children.size(); ++child_i) {
		destroy_id3_tree(node->children[child_i].pointer);
	}
	delete node;
}

int main(int argc, const char* argv[]) {
	data_t data_test; {
#if 0
		if(argc < 2) {
			argv[1] = "test.csv";
			fprintf(stderr, "warning: no .csv file given -- loading '%s' instead.\n", argv[1]);
		}
		fstream file_test;
		file_test.open(argv[1]);
		if(!file_test.is_open()) {
			fprintf(stderr, "failed to open '%s'\n", argv[1]);
			return 0;
		}
		stringstream file_ss; file_ss << file_test.rdbuf();
		if(!load_data(data_test, file_ss)) {
			fprintf(stderr, "failed to load '%s'\n", argv[1]);
			return 0;
		}
#else
	#if 1
		//
		// dataset taken from https://youtu.be/wL9aogTuZw8
		//
		const char* test_csv =
			"Gender,#Cars,Cost,Income,VEHICLE\n"
			"male,0,cheap,low,BUS\n"
			"male,1,cheap,medium,BUS\n"
			"female,1,cheap,medium,TRAIN\n"
			"female,0,cheap,low,BUS\n"
			"male,1,cheap,medium,BUS\n"
			"male,0,average,medium,TRAIN\n"
			"female,1,average,medium,TRAIN\n"
			"female,1,costly,high,CAR\n"
			"male,2,costly,medium,CAR\n"
			"female,2,costly,high,CAR";
	#else
		const char* test_csv =
			"Outlook,Temp,Humid,PLAY\n"
			"sunny,cool,high,TRUE\n"
			"rainy,mild,normal,TRUE\n"
			"ovrcast,hot,high,TRUE\n"
			"sunny,hot,normal,FALSE\n"
			"rainy,hot,normal,TRUE";
	#endif

		stringstream file_ss; file_ss << test_csv;
		if(!load_data(data_test, file_ss)) {
			fprintf(stderr, "failed to load internal data.\n");
			return 0;
		}
#endif
	}
	data_test.print();
	id3_node_t* root = create_id3_tree(data_test);

	printf("\n");
	print_id3_tree(root);

#if 0
	printf("\n");
	id3_node_t* node = root;
	while(node) {
		if(!node->children.size()) {
			printf("\nprediction: %s\n", node->label.c_str());
			break;
		}

		while(true) {
			printf("%s: ", node->label.c_str());
			string str;
			getline(cin, str);
			size_t i;
			for(i = 0; i < node->children.size(); ++i) {
				if(str == node->children[i].datum) break;
			}
			if(i >= node->children.size()) {
				printf("invalid\n");
				continue;
			}
			node = node->children[i].pointer;
			break;
		}
	}
#endif

	destroy_id3_tree(root);
	return 0;
}
